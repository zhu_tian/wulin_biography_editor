﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TaskEditor
{
    abstract class ZSeriable
    {
        public abstract void read(ZReader reader);

        public abstract void write(ZWriter writer);

        public virtual void beforeWrite()
        {

        }

        public virtual void beforeRead()
        {

        }
    }

    class ZMString
    {
        public byte[] buff;

        private Encoding zEncode;
        public ZMString(int bnum, Encoding code = null)
        {
            buff = new byte[bnum];
            if (code == null)
            {
                code = Encoding.Default;
            }
            zEncode = code;
        }
        public override string ToString()
        {
            return zEncode.GetString(buff);
        }
        public void zSetValue(string value)
        {
            byte[] data = zEncode.GetBytes(value);
            for (int i = 0; i < buff.Length; i++)
            {
                if (data.Length>i)
                {
                    buff[i] = data[i];
                }
                else
                {
                    buff[i] = 0;
                }
            }

        }
    }
}
