﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
namespace TaskEditor
{
    class EQUIPMENT_ADDON : ZSeriable
    {
        public uint id;
        public string name_64 = "";
        public int type;
        public int num_params;
        public int param1;
        public float _param1_1;
        public int param2;
        public int param3;
        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
        public override void read(ZReader reader)
        {

        }

        public override void write(ZWriter writer)
        {
        }
    }
    class EQUIPMENT_MAJOR_TYPE
    {
        public uint id;
        public string name_64 = "";
        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
    }
    class EQUIPMENT_SUB_TYPE
    {
        public uint id;
        public string name_64 = "";
        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
    }
    class EQUIPMENT_ESSENCE
    {
        public uint id;
        public uint id_major_type;
        public uint id_sub_type;
        public string name_64 = "";
        public byte[] new_equipment = new byte[4];
        public uint equip_type;
        public uint equip_mask;
        public int file_model_male;
        public int file_model_female;
        public int file_matter;
        public int file_icon;
        public uint equip_location;
        public uint action_type;
        public string show_level_32 = "";
        public int level;
        public uint character_combo_id;
        public uint require_gender;
        public int require_level;
        public uint respawn_required;
        public int hp;
        public int mp;
        public int min_dmg;
        public int max_dmg;
        public int defence;
        public int attack;
        public int armor;
        public float attack_range;
        public int price;
        public int shop_price;
        public int id_addon1;
        public int id_addon2;
        public int id_addon3;
        public int id_addon4;
        public int id_addon5;
        public int fee_estone;
        public int fee_install_pstone;
        public int fee_uninstall_pstone;
        public int fee_install_sstone;
        public int fee_uninstall_sstone;
        public uint id_estone;
        public int[] ehanced_value = new int[25];
        public int estone_level;
        public int hole_num;
        public int pile_num_max;
        public uint proc_type;
        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
    }
    class MEDICINE_MAJOR_TYPE
    {
        public uint id;
        public string name_64 = "";
        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
    }
    class MEDICINE_SUB_TYPE
    {
        public uint id;
        public string name_64 = "";
        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
    }
    class MEDICINE_ESSENCE
    {
        public uint id;
        public uint id_major_type;
        public uint id_sub_type;
        public string name_64 = "";
        public int file_matter;
        public int file_icon;
        public int require_level;
        public int cool_time;
        public uint only_in_war;
        public int type;
        public int[] hp = new int[4];
        public int[] mp = new int[4];
        public int price;
        public int shop_price;
        public int pile_num_max;
        public uint proc_type;
        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
    }
    class SKILLTOME_SUB_TYPE
    {
        public uint id;
        public string name_64 = "";
        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
    }
    class SKILLTOME_ESSENCE
    {
        public uint id;
        public uint id_sub_type;
        public string name_64 = "";
        public int file_matter;
        public int file_icon;
        public int price;
        public int shop_price;
        public int pile_num_max;
        public uint proc_type;
        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
    }
    class MATERIAL_MAJOR_TYPE
    {
        public uint id;
        public string name_64 = "";
        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
    }
    class MATERIAL_SUB_TYPE
    {
        public uint id;
        public string name_64 = "";
        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
    }
    class MATERIAL_ESSENCE
    {
        public uint id;
        public uint id_major_type;
        public uint id_sub_type;
        public string name_64 = "";
        public int file_matter;
        public int file_icon;
        public int price;
        public int shop_price;
        public int decompose_price;
        public int decompose_time;
        public uint element_id;
        public int element_num;
        public int pile_num_max;
        public uint proc_type;
        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
    }
    class REFINE_TICKET_ESSENCE
    {
        public uint id;
        public string name_64 = "";
        public int file_matter;
        public int file_icon;
        public float ext_reserved_prob;
        public float ext_succeed_prob;
        public int price;
        public int shop_price;
        public int pile_num_max;
        public uint proc_type;
        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
    }
    class TRANSMITROLL_ESSENCE
    {
        public uint id;
        public string name_64 = "";
        public int file_matter;
        public int file_icon;
        public int tag;
        public float x;
        public float y;
        public float z;
        public int price;
        public int shop_price;
        public int pile_num_max;
        public uint proc_type;
        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
    }
    class LUCKYROLL_ESSENCE
    {
        public uint id;
        public string name_64 = "";
        public int file_matter;
        public int file_icon;
        public int type;
        public int price;
        public int shop_price;
        public int pile_num_max;
        public uint proc_type;
        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
    }
    class TOWNSCROLL_ESSENCE
    {
        public uint id;
        public string name_64 = "";
        public int file_matter;
        public int file_icon;
        public float use_time;
        public int price;
        public int shop_price;
        public int pile_num_max;
        public uint proc_type;
        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
    }
    class REVIVESCROLL_ESSENCE
    {
        public uint id;
        public string name_64 = "";
        public int file_matter;
        public int file_icon;
        public float use_time;
        public int cool_time;
        public int price;
        public int shop_price;
        public int pile_num_max;
        public uint proc_type;
        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
    }
    class TASKMATTER_ESSENCE
    {
        public uint id;
        public string name_64 = "";
        public int file_icon;
        public int pile_num_max;
        public uint proc_type;
        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
    }
    class DROPTABLE_TYPE
    {
        public uint id;
        public string name_64 = "";
        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
    }
    class DROPTABLE_ESSENCE
    {
        public class DROP_DETAIL
        {
            public uint id;
            public float rate;
        }
        public uint id;
        public uint id_type;
        public string name_64 = "";
        public float[] num_to_drop = new float[5];
        public DROP_DETAIL[] drops = new DROP_DETAIL[64];
        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
    }
    class NPC_TALK_SERVICE
    {
        public uint id;
        public string name_64 = "";
        public uint id_dialog;
        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
    }
    class NPC_SELL_SERVICE
    {
        public class PAGE
        {
            public string page_title_16 = "";
            public uint[] id_goods = new uint[48];
        }

        public uint id;
        public string name_64 = "";
        public PAGE[] pages = new PAGE[8];
        public uint id_dialog;
        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
    }
    class NPC_BUY_SERVICE
    {
        public uint id;
        public string name_64 = "";
        public uint id_dialog;
        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
    }
    class NPC_TASK_IN_SERVICE
    {
        public uint id;
        public string name_64 = "";
        public uint[] id_tasks = new uint[32];
        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
    }
    class NPC_TASK_OUT_SERVICE
    {
        public uint id;
        public string name_64 = "";
        public uint id_task_set;
        public uint[] id_tasks = new uint[64];
        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
    }
    class NPC_TASK_MATTER_SERVICE
    {
        public class TASK
        {
            public class MATTER
            {
                public int id;
                public int num;
            }
            public int id;
            public MATTER[] matter = new MATTER[4];
        }

        public uint id;
        public string name_64 = "";
        public TASK[] tasks = new TASK[16];
        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
    }
    class NPC_HEAL_SERVICE
    {
        public uint id;
        public string name_64 = "";
        public uint id_dialog;
        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
    }
    class NPC_TRANSMIT_SERVICE
    {
        public class TARGET
        {
            public int id_world;
            public string name_32 = "";
            public float x;
            public float y;
            public float z;
            public int fee;
            public int required_level;
        }

        public uint id;
        public string name_64 = "";
        public TARGET[] targets = new TARGET[16];
        public uint id_dialog;
        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
    }
    class NPC_PROXY_SERVICE
    {
        public uint id;
        public string name_64 = "";
        public uint id_dialog;
        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
    }
    class NPC_STORAGE_SERVICE
    {
        public uint id;
        public string name_64 = "";
        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
    }
    class NPC_TYPE
    {
        public uint id;
        public string name_64 = "";
        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
    }
    class NPC_ESSENCE
    {
        public uint id;
        public string name_64 = "";
        public uint id_type;
        public float refresh_time;
        public uint attack_rule;
        public int file_model;
        public uint id_src_monster;
        public string hello_msg_512 = "";
        public uint id_to_discover;
        public uint domain_related;
        public uint guard_npc;
        public uint id_talk_service;
        public uint id_sell_service;
        public uint id_gshop_majortype;
        public uint id_buy_service;
        public uint id_task_out_service;
        public uint id_task_in_service;
        public uint id_task_matter_service;
        public uint id_heal_service;
        public uint id_transmit_service;
        public uint id_proxy_service;
        public uint id_storage_service;
        public uint id_war_towerbuild_service;
        public uint id_resetprop_service;
        public uint id_equipbind_service;
        public uint id_equipdestroy_service;
        public uint id_equipundestroy_service;
        public uint combined_services;
        public uint id_item_cash_service;
        public uint has_pkvalue_service;
        public int fee_per_pkvalue;
        public bool service_install;
        public bool service_uninstall;
        public bool service_temp1;
        public bool service_temp2;
        public uint id_mine;
        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
    }
    class MONSTER_TYPE
    {
        public uint id;
        public string name_64 = "";
        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
    }
    class MONSTER_ESSENCE
    {
        public class SKILL
        {
            public uint id_skill;
            public int level;
        }

        public uint id;
        public uint id_type;
        public string name_64 = "";
        public string prop_32 = "";
        public string desc_32 = "";
        public uint faction;
        public uint enemy_faction;
        public uint monster_faction;
        public int file_model;
        public float size;
        public uint id_strategy;
        public int level;
        public int show_level;
        public uint is_boss;
        public uint killed_exp;
        public uint killed_drop;
        public uint immune_type;
        public float sight_range;
        public float attack_range;
        public uint aggressive_mode;
        public uint monster_faction_ask_help;
        public uint monster_faction_can_help;
        public float aggro_range;
        public float aggro_time;
        public float dead_aggro_time;
        public uint patroll_mode;
        public uint stand_mode;
        public float walk_speed;
        public float run_speed;
        public uint common_strategy;
        public uint after_death;
        public int exp;
        public int money_average;
        public int money_var;
        public int hp;
        public int mp;
        public int dmg;
        public int defense;
        public int attack;
        public int armor;
        public int crit_rate;
        public int crit_damage;
        public int anti_stunt;
        public int anti_weak;
        public int anti_slow;
        public int anti_silence;
        public int anti_sleep;
        public int anti_twist;
        public float lvlup_exp;
        public float lvlup_money_average;
        public float lvlup_money_var;
        public float lvlup_hp;
        public float lvlup_mp;
        public float lvlup_dmg;
        public float lvlup_defense;
        public float lvlup_attack;
        public float lvlup_armor;
        public float lvlup_crit_rate;
        public float lvlup_crit_damage;
        public float lvlup_anti_stunt;
        public float lvlup_anti_weak;
        public float lvlup_anti_slow;
        public float lvlup_anti_silence;
        public float lvlup_anti_sleep;
        public float lvlup_anti_twist;
        public int hp_gen1;
        public int hp_gen2;
        public int mp_gen1;
        public int mp_gen2;
        public uint role_in_war;
        public int drop_times;
        public int id_drop_table;
        public SKILL[] skills = new SKILL[32];
        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
    }
    class RECIPE_MAJOR_TYPE
    {
        public uint id;
        public string name_64 = "";
        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
    }
    class RECIPE_SUB_TYPE
    {
        public uint id;
        public string name_64 = "";
        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
    }
    class RECIPE_ESSENCE
    {
        public class PRODUCT
        {

            public float probability;
            public uint id_to_make;
            public int min_num_make;
            public int max_num_make;
        }
        public class MATERIAL
        {
            public uint id;
            public int num;
        }

        public uint id;
        public uint id_major_type;
        public uint id_sub_type;
        public string name_64 = "";
        public int level;
        public uint special_recipe;
        public PRODUCT[] products = new PRODUCT[3];
        public int price;
        public float duration;
        public MATERIAL[] materials = new MATERIAL[6];
        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
    }
    class ENEMY_FACTION_CONFIG
    {
        public uint id;
        public string name_64 = "";
        public uint[] enemy_factions = new uint[32];
        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
    }
    class CHARRACTER_CLASS_CONFIG
    {
        public uint id;
        public string name_64 = "";
        public uint character_class_id;
        public uint faction;
        public uint enemy_faction;
        public uint respawn_required;
        public int level_required;
        public int hp;
        public int mp;
        public int dmg;
        public int defense;
        public int attack;
        public int armor;
        public float crit_rate;
        public float crit_damage;
        public int anti_stunt;
        public int anti_weak;
        public int anti_slow;
        public int anti_silence;
        public int anti_sleep;
        public int anti_twist;
        public float lvlup_hp;
        public float lvlup_mp;
        public float lvlup_dmg;
        public float lvlup_defense;
        public float lvlup_attack;
        public float lvlup_armor;
        public float lvlup_crit_rate;
        public float lvlup_crit_damage;
        public float lvlup_anti_stunt;
        public float lvlup_anti_weak;
        public float lvlup_anti_slow;
        public float lvlup_anti_silence;
        public float lvlup_anti_sleep;
        public float lvlup_anti_twist;
        public float walk_speed;
        public float run_speed;
        public int hp_gen1;
        public int hp_gen2;
        public int hp_gen3;
        public int hp_gen4;
        public int mp_gen1;
        public int mp_gen2;
        public int mp_gen3;
        public int mp_gen4;
        public uint character_combo_id;
        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
    }
    class PARAM_ADJUST_CONFIG
    {
        public class Team_adjus
        {
            public float adjust_exp;
        }

        public class Level_diff_adjust
        {
            public int level_diff;
            public float adjust_exp;
            public float adjust_money;
            public float adjust_matter;
            public float adjust_attack;
        }

        public class Level_diff_produce
        {
            public int level_diff;
            public float adjust_exp;
        }

        public uint id;
        public string name_64 = "";
        public Level_diff_adjust[] level_diff_adjust = new Level_diff_adjust[16];
        public Team_adjus[] team_adjust = new Team_adjus[11];
        public Team_adjus[] team_profession_adjust = new Team_adjus[9];
        public Level_diff_produce[] level_diff_produce = new Level_diff_produce[9];
        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
    }
    class TASKDICE_ESSENCE
    {
        public class TASKLIST
        {
            public uint id;
            public float probability;
        }
        public uint id;
        public string name_64 = "";
        public int file_matter;
        public int file_icon;
        public TASKLIST[] task_lists = new TASKLIST[8];
        public int pile_num_max;
        public uint proc_type;
        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
    }
    class TASKNORMALMATTER_ESSENCE
    {
        public uint id;
        public string name_64 = "";
        public int file_matter;
        public int file_icon;
        public int price;
        public int shop_price;
        public int pile_num_max;
        public uint proc_type;
        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
    }
    class PLAYER_LEVELEXP_CONFIG
    {
        public uint id;
        public string name_64 = "";
        public double[] exp = new double[200];
        public double[] pet_exp = new double[200];
        public int[] prod_exp_need = new int[10];
        public int[] prod_exp_gained = new int[100];
        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
    }
    class MINE_TYPE
    {
        public uint id;
        public string name_64 = "";
        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
    }
    class MINE_ESSENCE
    {
        public class MATERIAL
        {
            public uint id;
            public float probability;
        }
        public class NPCGEN
        {
            public uint id_monster;
            public int num;
            public float radius;
        }
        public class AGGROS
        {
            public uint monster_faction;
            public float radius;
            public int num;
        }
        public uint id;
        public uint id_type;
        public string name_64 = "";
        public uint level;
        public uint level_required;
        public uint id_equipment_required;
        public uint eliminate_tool;
        public uint time_min;
        public uint time_max;
        public int exp;
        public int skillpoint;
        public int file_model;
        public MATERIAL[] materials = new MATERIAL[16];
        public int num1;
        public float probability1;
        public int num2;
        public float probability2;
        public uint task_in;
        public uint task_out;
        public uint uninterruptable;
        public NPCGEN[] npcgen = new NPCGEN[4];
        public AGGROS[] aggros = new AGGROS[1];
        public uint role_in_war;
        public uint permenent;
        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
    }
    class GM_GENERATOR_TYPE
    {
        public uint id;
        public string name_64 = "";
        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
    }
    class GM_GENERATOR_ESSENCE
    {
        public uint id;
        public uint id_type;
        public string name_64 = "";
        public int file_matter;
        public int file_icon;
        public uint id_object;
        public int pile_num_max;
        public uint proc_type;
        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
    }
    class FIREWORKS_ESSENCE
    {
        public uint id;
        public string name_64 = "";
        public int file_matter;
        public int file_icon;
        public int file_fw;
        public int level;
        public int time_to_fire;
        public int price;
        public int shop_price;
        public int pile_num_max;
        public uint proc_type;
        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
    }
    class NPC_WAR_TOWERBUILD_SERVICE
    {
        public class BUILD_INFO
        {
            public int id_in_build;
            public int id_buildup;
            public int id_object_need;
            public int time_use;
            public int fee;
        }
        public uint id;
        public string name_64 = "";
        public BUILD_INFO[] build_info = new BUILD_INFO[4];
        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
    }
    class PLAYER_SECONDLEVEL_CONFIG
    {
        public uint id;
        public string name_64 = "";
        public float[] exp_lost = new float[256];
        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
    }
    class NPC_RESETPROP_SERVICE
    {
        public class PROP_ENTRY
        {
            public int id_object_need;
            public int resetprop_type;
        }
        public uint id;
        public string name_64 = "";
        public PROP_ENTRY[] prop_entry = new PROP_ENTRY[6];
        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
    }
    class ESTONE_ESSENCE
    {
        public class EFFECT
        {
            public uint equip_mask;
            public uint effect_addon_type;
        }
        public uint id;
        public string name_64 = "";
        public int file_matter;
        public int file_icon;
        public EFFECT[] effects = new EFFECT[4];
        public int price;
        public int shop_price;
        public int pile_num_max;
        public uint proc_type;
        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
    }
    class PSTONE_ESSENCE
    {
        public uint id;
        public string name_64 = "";
        public int file_matter;
        public int file_icon;
        public uint type;
        public uint equip_mask;
        public uint effect_addon_id;
        public int price;
        public int shop_price;
        public int pile_num_max;
        public uint proc_type;
        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
    }
    class SSTONE_ESSENCE
    {
        public uint id;
        public string name_64 = "";
        public int file_matter;
        public int file_icon;
        public uint equip_mask;
        public uint skill_id;
        public int skill_level;
        public int price;
        public int shop_price;
        public int pile_num_max;
        public uint proc_type;
        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
    }
    class RECIPEROLL_MAJOR_TYPE
    {
        public uint id;
        public string name_64 = "";
        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
    }
    class RECIPEROLL_SUB_TYPE
    {
        public uint id;
        public string name_64 = "";
        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
    }
    class RECIPEROLL_ESSENCE
    {
        public uint id;
        public uint id_major_type;
        public uint id_sub_type;
        public string name_64 = "";
        public int file_matter;
        public int file_icon;
        public uint id_recipe;
        public int price;
        public int shop_price;
        public int pile_num_max;
        public uint proc_type;
        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
    }
    class SUITE_ESSENCE : ZSeriable
    {
        public uint id;
        public string name_64 = "";
        public int max_equips;
        public int[] equipments = new int[14];
        public int[] addons = new int[13];
        public byte[] file_gfx_128 = new byte[128];
        public int hh_type;
        public string _file_gfx_fix = null;
        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
        public override void read(ZReader reader)
        {
            _file_gfx_fix = Encoding.Default.GetString(file_gfx_128);
        }

        public override void beforeWrite()
        {
            byte[] buff = new byte[128];
            byte[] data = Encoding.Default.GetBytes(_file_gfx_fix);
            for (int i = 0; i < 128; i++)
            {
                if (data.Length > i)
                {
                    buff[i] = data[i];
                }
            }
            file_gfx_128 = buff;
        }
        public override void write(ZWriter writer)
        {

        }
    }
    class DOUBLE_EXP_ESSENCE
    {
        public uint id;
        public string name_64 = "";
        public int file_matter;
        public int file_icon;
        public int double_exp_time;
        public int price;
        public int shop_price;
        public int pile_num_max;
        public uint proc_type;
        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
    }

    class DESTROYING_ESSENCE
    {
        public uint id;
        public string name_64 = "";
        public int file_matter;
        public int file_icon;
        public int price;
        public int shop_price;
        public int pile_num_max;
        public uint proc_type;
        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
    }
    class NPC_EQUIPBIND_SERVICE
    {
        public class INFO
        {
            public int id_object_need;
            public int price;
            public int bind_time;
        }
        public uint id;
        public string name_64 = "";
        public INFO[] info = new INFO[16];
        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
    }
    class NPC_EQUIPDESTROY_SERVICE
    {
        public uint id;
        public string name_64 = "";
        public int id_object_need;
        public int price;
        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
    }
    class NPC_EQUIPUNDESTROY_SERVICE
    {
        public uint id;
        public string name_64 = "";
        public int id_object_need;
        public int price;
        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
    };
    class NPC_ITEM_CASH_SERVICE
    {
        public class PAGES
        {
            public class GOODS_LIST
            {
                public int item_goods;
                public int item_cash;
                public int item_cash_num;
            }
            public string page_title_16 = "";
            public GOODS_LIST[] goods_list = new GOODS_LIST[10];
        }
        public uint id;
        public string name_64 = "";
        public PAGES[] pages = new PAGES[3];
        public uint id_dialog;
        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
    }
    class SKILLMATTER_ESSENCE
    {
        public uint id;
        public string name_64 = "";
        public int file_matter;
        public int file_icon;
        public int level_required;
        public int id_skill;
        public int level_skill;
        public uint only_in_war;
        public int cool_type;
        public int price;
        public int shop_price;
        public int pile_num_max;
        public uint proc_type;
        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
    }
    class VEHICLE_ESSENCE
    {
        public uint id;
        public string name_64 = "";
        public int file_model;
        public int file_matter;
        public int file_icon;
        public int level_required;
        public uint respawn_required;
        public uint only_in_war;
        public float speed;
        public int hp;
        public int mp;
        public int min_dmg;
        public int max_dmg;
        public int defence;
        public int attack;
        public int armor;
        public float attack_range;
        public int[] id_addon = new int[5];
        public int price;
        public int shop_price;
        public int pile_num_max;
        public uint proc_type;
        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
    }
    class COUPLE_JUMPTO_ESSENCE
    {
        public uint id;
        public string name_64 = "";
        public int file_matter;
        public int file_icon;
        public int price;
        public int shop_price;
        public int pile_num_max;
        public uint proc_type;
        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
    }
    class LOTTERY_ESSENCE
    {
        public class CANDIDATE
        {
            public string desc_32 = "";
            public int icon;
        }
        public uint id;
        public string name_64 = "";
        public int file_matter;
        public int file_icon;
        public int dice_count;
        public CANDIDATE[] candidates = new CANDIDATE[32];
        public int price;
        public int shop_price;
        public int pile_num_max;
        public uint proc_type;
        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
    }
    class CAMRECORDER_ESSENCE
    {
        public uint id;
        public string name_64 = "";
        public int file_matter;
        public int file_icon;
        public int price;
        public int shop_price;
        public int pile_num_max;
        public uint proc_type;
        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
    }
    class TITLE_PROP_CONFIG
    {
        public uint id;
        public string name_64 = "";
        public uint id_title;
        public uint[] id_addons = new uint[3];
        public int addons_effect_forever;
        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
    }
    class SPECIAL_ID_CONFIG
    {
        public uint id;
        public string name_64 = "";
        public float monster_drop_prob;
        public int id_money_matter;
        public int id_speaker;
        public int id_destroying_matter;
        public int id_attacker_droptable;
        public int id_defender_droptable;
        public int fee_pet_lingyang;
        public int fee_pet_jianding;
        public int fee_pet_jichufuhuo;
        public int fee_pet_jichuhuifu;
        public int fee_pet_fengyin;
        public int fee_pet_gaiming;
        public int fee_pet_xuexijineng;
        public int fee_pet_yiwangjineng;
        public int fee_pet_hecheng;
        public int fee_pet_bianxing;
        public int fee_pet_transform;
        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
    }
    class TEXT_FIREWORKS_ESSENCE
    {
        public uint id;
        public string name_64 = "";
        public int file_matter;
        public int file_icon;
        public int file_fw;
        public int level;
        public string char_can_use_512 = "";
        public int max_char;
        public int price;
        public int shop_price;
        public int pile_num_max;
        public uint proc_type;
        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
    }
    class AI_HELPER_ESSENCE
    {
        public uint id;
        public string name_64 = "";
        public int file_matter;
        public int file_icon;
        public int duration;
        public int price;
        public int shop_price;
        public int pile_num_max;
        public uint proc_type;
        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
    }
    class PET_BEDGE_ESSENCE
    {
        public class AA
        {
            public int main_prop;
            public float init_value_min;
            public float init_value_max;
            public float merge_value_max;
            public float p_value_min;
            public float p_value_max;
            public float merge_p_value_max;
        }
        public uint id;
        public string name_64 = "";
        public int file_matter;
        public int file_icon;
        public int[] file_head_icon = new int[2];
        public int[] file_to_shown = new int[16];
        public float size;
        public int pet_type;
        public uint pet_food_mask;
        public int level;
        public int life;
        public float sight_range;
        public float attack_range;
        public float aggro_range;
        public float aggro_time;
        public uint stand_mode;
        public float walk_speed;
        public float run_speed;
        public int max_skills;
        public int min_skills;
        public AA[] props = new AA[13];
        public int price;
        public int shop_price;
        public int pile_num_max;
        public uint proc_type;
        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
    }
    class PET_FOOD_ESSENCE
    {
        public uint id;
        public string name_64 = "";
        public int file_matter;
        public int file_icon;
        public int pet_level_required;
        public int pet_food_type;
        public int food_usage;
        public int food_value;
        public int price;
        public int shop_price;
        public int pile_num_max;
        public uint proc_type;
        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
    }
    class PET_SKILL_ESSENCE
    {
        public uint id;
        public string name_64 = "";
        public int file_matter;
        public int file_icon;
        public int id_skill;
        public int skill_level;
        public uint pet_type_mask;
        public int price;
        public int shop_price;
        public int pile_num_max;
        public uint proc_type;
        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
    }
    class PET_LIFEGEN_ESSENCE
    {
        public uint id;
        public string name_64 = "";
        public int file_matter;
        public int file_icon;
        public int life_value;
        public int price;
        public int shop_price;
        public int pile_num_max;
        public uint proc_type;
        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
    }

    class CHANGESHAPE_ESSENCE
    {
        public class AA
        {
            public int id_skill;
            public int level;
        }
        public uint id;
        public string name_64 = "";
        public int file_matter;
        public int file_icon;
        public int file_model;
        public string desc_512 = "";
        public int duration;
        public int level_required;
        public uint stand_mode;
        public float height_delta;
        public float speed;
        public int can_attack;
        public ZMString act_buff_32 = new ZMString(32, Encoding.Default);
        public ZMString act_attack_32 = new ZMString(32, Encoding.Default);
        public AA[] skills = new AA[3];
        public int[] id_addons = new int[6];
        public int price;
        public int shop_price;
        public int pile_num_max;
        public uint proc_type;
        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
    }
    class PET_ARMOR_ESSENCE
    {
        public class AA
        {
            public int maximum;
            public int minimum;
        }
        public class BB
        {
            public float maximum;
            public float minimum;
        }
        public uint id;
        public string name_64 = "";
        public int file_matter;
        public int file_icon;
        public string desc_32 = "";
        public AA[] int_props = new AA[11];
        public BB[] float_props = new BB[2];
        public int price;
        public int shop_price;
        public int pile_num_max;
        public uint proc_type;
        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
    }
    class ADV_TRANSROLL_ESSENCE
    {
        public class AA
        {
            public string name_32 = "";
            public float pos_x;
            public float pos_y;
            public float pos_z;
        }
        public class BB
        {
            public string name_32 = "";
            public int id_map;
            public AA[] positions = new AA[8];
        }

        public uint id;
        public string name_64 = "";
        public int file_matter;
        public int file_icon;
        public BB[] infos = new BB[10];
        public int price;
        public int shop_price;
        public int pile_num_max;
        public uint proc_type;
        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
    }
    class RADAR_ESSENCE
    {
        public uint id;
        public string name_64 = "";
        public int file_matter;
        public int file_icon;
        public uint is_enemy;
        public int price;
        public int shop_price;
        public int pile_num_max;
        public uint proc_type;
        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
    }
    class LITERARY_DOUBLE_EXP
    {
        public uint id;
        public string name_64 = "";
        public int file_matter;
        public int file_icon;
        public int price;
        public int shop_price;
        public int pile_num_max;
        public uint proc_type;
        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
    }
    class NEW_LOTTERY_ESSENCE
    {
        public class AA
        {
            public uint id;
            public int count;
        }
        public uint id;
        public string name_64 = "";
        public int file_matter;
        public int file_icon;
        public uint bet_item_id;
        public int rank_id;
        public AA[] candidates = new AA[32];
        public int price;
        public int shop_price;
        public int pile_num_max;
        public uint proc_type;
        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
    }
    class LOTTERY_BET_ITEM_ESSENCE
    {
        public uint id;
        public string name_64 = "";
        public int file_matter;
        public int file_icon;
        public int price;
        public int shop_price;
        public int pile_num_max;
        public uint proc_type;
        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
    }
    class STILETTO_TOOL_ESSENCE
    {
        public uint id;
        public string name_64 = "";
        public int file_matter;
        public int file_icon;
        public float probability;
        public int hole_index;
        public int level;
        public int cost;
        public int price;
        public int shop_price;
        public int pile_num_max;
        public uint proc_type;
        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
    }
    class STILETTO_HELPER_ESSENCE
    {
        public uint id;
        public string name_64 = "";
        public int file_matter;
        public int file_icon;
        public float probability;
        public int price;
        public int shop_price;
        public int pile_num_max;
        public uint proc_type;
        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
    }
    class INLAY_STONE_ESSENCE
    {
        public uint id;
        public string name_64 = "";
        public int file_matter;
        public int file_icon;
        public float probability;
        public int level;
        public int quality_level;
        public uint equip_mask;
        public uint[] id_effect_addon = new uint[6];
        public int cost;
        public int price;
        public int shop_price;
        public int pile_num_max;
        public uint proc_type;
        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
    }
    class INLAY_HELPER_ESSENCE
    {
        public uint id;
        public string name_64 = "";
        public int file_matter;
        public int file_icon;
        public float succeed_prob;
        public float reserved_prob;
        public int price;
        public int shop_price;
        public int pile_num_max;
        public uint proc_type;
        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
    }
    class ANTI_INLAY_ESSENCE
    {
        public uint id;
        public string name_64 = "";
        public int file_matter;
        public int file_icon;
        public int level;
        public int cost;
        public int price;
        public int shop_price;
        public int pile_num_max;
        public uint proc_type;
        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
    }
    class INLAY_CHARM_ESSENCE
    {
        public uint id;
        public string name_64 = "";
        public int inlay_stone_num;
        public uint[] id_inlay_stone = new uint[6];
        public uint[] id_effect_addon = new uint[6];
        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
    }
    class TRANSFORM_STONE_ESSENCE
    {
        public uint id;
        public string name_64 = "";
        public int file_matter;
        public int file_icon;
        public int property;
        public float probability;
        public int level;
        public int price;
        public int shop_price;
        public int pile_num_max;
        public uint proc_type;
        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
    }
    class TRANSFORM_HELPER_ESSENCE
    {
        public uint id;
        public string name_64 = "";
        public int file_matter;
        public int file_icon;
        public float probability;
        public int price;
        public int shop_price;
        public int pile_num_max;
        public uint proc_type;
        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
    }
    class TRANSFORM_RECIPE_ESSENCE
    {
        public class AA
        {
            public int id;
            public int num;
        }

        public uint id;
        public string name_64 = "";
        public int id_make;
        public int id_main_material;
        public AA[] material = new AA[3];
        public int price;
        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
    }
    class INSTANCE_CONFIG
    {
        public class AA
        {
            public int region_repu_index;
            public int min_region_repu;
            public int cost_region_repu;
        }
        public class BB
        {
            public int score_gap_value;
            public int open_ctrl;
        }
        public class CC
        {
            public int kill_monster_id;
            public int kill_monster_score_add;
        }
        public class DD
        {
            public int score_reach;
            public int score_reach_open_ctrl;
        }
        public class EE
        {
            public int[] enter_delv_task = new int[2];
            public int succ_open_ctrl;
            public int fail_open_ctrl;
            public int end_open_ctrl;
            public int draw_open_ctrl;
            public int end_delv_task;
            public int succ_delv_task;
            public int fail_delv_task;
            public int draw_delv_task;
            public int succ_delv_item;
            public int succ_delv_item_num;
            public int fail_delv_item;
            public int fail_delv_item_num;
            public int victory_type;
            public int score_reach_need;
            public int score_gap_need;
            public CC[] kill_monster_score = new CC[3];
            public int kill_opponent_score_add;
            public int collect_item_id;
            public int collect_item_score_add;
            public DD[] score_reach = new DD[2];
            public int delv_item_NO1;
            public int delv_item_num_NO1;
            public int delv_item_NO2_5;
            public int delv_item_num_NO2_5;
            public int delv_item_NO6_10;
            public int delv_item_num_NO6_10;
        }
        public uint id;
        public string name_64 = "";
        public string desc_512 = "";
        public int common;
        public int tag;
        public int type;
        public int file_icon;
        public int min_player_num;
        public int max_player_num;
        public int min_player_num_defender;
        public int max_player_num_defender;
        public int midway_join;
        public int inst_time;
        public int inst_revive_time;
        public int prepare_time;
        public int close_delay_time;
        public int no_players_delay_time;
        public AA[] region_repu_req = new AA[3];
        public int min_level_req;
        public int max_level_req;
        public int min_respawn_req;
        public int max_respawn_req;
        public int title_req;
        public int god_req;
        public int item_req;
        public int item_num_req;
        public int item_req_defender;
        public int item_num_req_defender;
        public int finish_task;
        public int finish_task_cnt;
        public int[] enter_delv_task = new int[2];
        public int prepare_open_ctrl;
        public int start_open_ctrl;
        public int end_open_ctrl;
        public int end_delv_task;
        public int end_delv_item;
        public int end_delv_item_num;
        public int end_result;
        public BB[] score_gap_open_ctrl = new BB[2];
        public EE[] sides = new EE[2];
        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
    }
    class MERGE_RECIPE_ESSENCE
    {
        public struct AA
        {
            public float probability;
            public int id;
            public int num;
        };
        public struct BB
        {
            public int id;
            public float probability;
        };
        public uint id;
        public string name_64 = "";
        public AA[] makes = new AA[2];
        public int id_main;
        public int fee;
        public BB[] helpers = new BB[4];

        public override string ToString()
        {
            return $"<{id}>{name_64}";
        }
    };
    class ZElementVector<T> : ZSeriable where T : class
    {
        public int DataSize = 0;
        public int Count = 0;
        public List<T> _data = new List<T>();
        public override void read(ZReader reader)
        {
            for (int i = 0; i < Count; i++)
            {
                long pos = reader.POS;
                _data.Add(reader.readt<T>());
                pos = reader.POS - pos;
                if (pos != DataSize)
                {
                    DialogResult dr = MessageBox.Show($"类型大小不匹配-{typeof(T).Name}\r\n实际大小:{pos}，获取大小：{DataSize}", "是否结束程序", MessageBoxButtons.OKCancel);
                    if (dr == DialogResult.Cancel)
                    {
                        Application.Exit();
                    }
                }
                //if (reader.POS>13124852)
                //{
                //    int a = 0;
                //    a++;
                //}
            }

        }
        public override void beforeWrite()
        {
            Count = _data.Count;
            T t = Activator.CreateInstance<T>();

            int n = caultsize(t);
            if (DataSize != 0 && n != DataSize)
            {
                DialogResult dr = MessageBox.Show($"类型大小不匹配-{typeof(T).Name}\r\n实际大小:{DataSize}，设置大小：{n}", "是否结束程序", MessageBoxButtons.OKCancel);
                if (dr == DialogResult.OK)
                {
                    Application.Exit();
                }
            }
        }
        public override void write(ZWriter writer)
        {
            foreach (var item in _data)
            {
                writer.writeObject(item);
            }
        }
        private int caultsize(object obj, string extrname = null)
        {
            Type t = obj.GetType();
            int size = 0;
            if (t.IsArray)
            {
                Array a = obj as Array;
                if (a.Length > 0)
                {
                    object ele = a.GetValue(0);
                    if (ele == null)
                    {
                        ele = Activator.CreateInstance(t.GetElementType());
                    }
                    int n = caultsize(ele);
                    size = n * a.Length;
                    return size;
                }

            }
            else if (t.FullName.StartsWith("System"))
            {
                switch (t.Name)
                {
                    case "Double":
                        size += 8;
                        break;
                    case "Char":
                        size += 2;
                        break;
                    case "Single":
                    case "UInt32":
                    case "Int32":
                        size += 4;
                        break;
                    case "UInt16":
                    case "Int16":
                        size += 2;
                        break;
                    case "Byte":
                    case "Boolean":
                        size += 1;
                        break;
                    case "String":
                        string[] num = extrname.Split(new char[] { '_' });
                        int bnum = int.Parse(num[num.Length - 1]);
                        size += bnum;
                        break;
                    default:
                        throw new Exception("未处理类型:" + t.Name);
                }


            }
            else
            {
                var fieds = t.GetFields();
                foreach (var item in fieds)
                {
                    if (item.Name.StartsWith("_"))
                    {
                        continue;
                    }
                    size += caultsize(item.GetValue(obj), item.Name);
                }
            }
            return size;
        }

        public override string ToString()
        {
            return $"[{Count}]";
        }
    }
    class ZElementFile
    {
        public uint version = 268435527;
        public int timeType = 0;
        public ZElementVector<EQUIPMENT_ADDON> 装备属性 = new ZElementVector<EQUIPMENT_ADDON>();
        public ZElementVector<EQUIPMENT_MAJOR_TYPE> 装备属性大类 = new ZElementVector<EQUIPMENT_MAJOR_TYPE>();
        public ZElementVector<EQUIPMENT_SUB_TYPE> 装备属性小类 = new ZElementVector<EQUIPMENT_SUB_TYPE>();
        public ZElementVector<EQUIPMENT_ESSENCE> 装备 = new ZElementVector<EQUIPMENT_ESSENCE>();
        public ZElementVector<REFINE_TICKET_ESSENCE> refine_ticket_essence_array = new ZElementVector<REFINE_TICKET_ESSENCE>();
        public ZElementVector<MEDICINE_MAJOR_TYPE> 药品大类 = new ZElementVector<MEDICINE_MAJOR_TYPE>();
        public ZElementVector<MEDICINE_SUB_TYPE> 药品小类 = new ZElementVector<MEDICINE_SUB_TYPE>();
        public ZElementVector<MEDICINE_ESSENCE> 药品 = new ZElementVector<MEDICINE_ESSENCE>();
        public ZElementVector<MATERIAL_MAJOR_TYPE> 材料大类 = new ZElementVector<MATERIAL_MAJOR_TYPE>();
        public ZElementVector<MATERIAL_SUB_TYPE> 材料小类 = new ZElementVector<MATERIAL_SUB_TYPE>();
        public ZElementVector<MATERIAL_ESSENCE> 材料 = new ZElementVector<MATERIAL_ESSENCE>();
        public ZElementVector<SKILLTOME_SUB_TYPE> skilltome_sub_type_array = new ZElementVector<SKILLTOME_SUB_TYPE>();
        public ZElementVector<SKILLTOME_ESSENCE> skilltome_essence_array = new ZElementVector<SKILLTOME_ESSENCE>();
        public ZElementVector<TRANSMITROLL_ESSENCE> 传送门 = new ZElementVector<TRANSMITROLL_ESSENCE>();
        public ZElementVector<LUCKYROLL_ESSENCE> 彩票 = new ZElementVector<LUCKYROLL_ESSENCE>();
        public ZElementVector<TOWNSCROLL_ESSENCE> townscroll_essence_array = new ZElementVector<TOWNSCROLL_ESSENCE>();
        public ZElementVector<REVIVESCROLL_ESSENCE> revivescroll_essence_array = new ZElementVector<REVIVESCROLL_ESSENCE>();
        public ZElementVector<TASKMATTER_ESSENCE> taskmatter_essence_array = new ZElementVector<TASKMATTER_ESSENCE>();
        public ZElementVector<DROPTABLE_TYPE> 掉落类型 = new ZElementVector<DROPTABLE_TYPE>();
        public ZElementVector<DROPTABLE_ESSENCE> 掉落表 = new ZElementVector<DROPTABLE_ESSENCE>();
        public ZElementVector<MONSTER_TYPE> 怪物类型 = new ZElementVector<MONSTER_TYPE>();
        public ZElementVector<MONSTER_ESSENCE> 怪物 = new ZElementVector<MONSTER_ESSENCE>();
        public ZElementVector<NPC_TALK_SERVICE> NPC对话服务 = new ZElementVector<NPC_TALK_SERVICE>();
        public ZElementVector<NPC_SELL_SERVICE> NPC出售服务 = new ZElementVector<NPC_SELL_SERVICE>();
        public ZElementVector<NPC_BUY_SERVICE> NPC收购服务 = new ZElementVector<NPC_BUY_SERVICE>();
        public ZElementVector<NPC_TASK_IN_SERVICE> NPC发布任务 = new ZElementVector<NPC_TASK_IN_SERVICE>();
        public ZElementVector<NPC_TASK_OUT_SERVICE> NPC回收任务 = new ZElementVector<NPC_TASK_OUT_SERVICE>();
        public ZElementVector<NPC_TASK_MATTER_SERVICE> npc_task_matter_service_array = new ZElementVector<NPC_TASK_MATTER_SERVICE>();
        public ZElementVector<NPC_HEAL_SERVICE> npc_heal_service_array = new ZElementVector<NPC_HEAL_SERVICE>();
        public ZElementVector<NPC_TRANSMIT_SERVICE> NPC传送服务 = new ZElementVector<NPC_TRANSMIT_SERVICE>();
        public ZElementVector<NPC_PROXY_SERVICE> npc_proxy_service_array = new ZElementVector<NPC_PROXY_SERVICE>();
        public ZElementVector<NPC_STORAGE_SERVICE> npc_storage_service_array = new ZElementVector<NPC_STORAGE_SERVICE>();
        public ZElementVector<NPC_TYPE> NPC类型 = new ZElementVector<NPC_TYPE>();
        public ZElementVector<NPC_ESSENCE> NPC = new ZElementVector<NPC_ESSENCE>();
        public ZElementVector<RECIPE_MAJOR_TYPE> recipe_major_type_array = new ZElementVector<RECIPE_MAJOR_TYPE>();
        public ZElementVector<RECIPE_SUB_TYPE> recipe_sub_type_array = new ZElementVector<RECIPE_SUB_TYPE>();
        public ZElementVector<RECIPE_ESSENCE> recipe_essence_array = new ZElementVector<RECIPE_ESSENCE>();
        public ZElementVector<ENEMY_FACTION_CONFIG> enemy_faction_config_array = new ZElementVector<ENEMY_FACTION_CONFIG>();
        public ZElementVector<CHARRACTER_CLASS_CONFIG> character_class_config_array = new ZElementVector<CHARRACTER_CLASS_CONFIG>();
        public ZElementVector<PARAM_ADJUST_CONFIG> param_adjust_config_array = new ZElementVector<PARAM_ADJUST_CONFIG>();
        public ZElementVector<TASKDICE_ESSENCE> taskdice_essence_array = new ZElementVector<TASKDICE_ESSENCE>();
        public ZElementVector<TASKNORMALMATTER_ESSENCE> tasknormalmatter_essence_array = new ZElementVector<TASKNORMALMATTER_ESSENCE>();
        public ZElementVector<PLAYER_LEVELEXP_CONFIG> player_levelexp_config_array = new ZElementVector<PLAYER_LEVELEXP_CONFIG>();
        public ZElementVector<MINE_TYPE> mine_type_array = new ZElementVector<MINE_TYPE>();
        public ZElementVector<MINE_ESSENCE> mine_essence_array = new ZElementVector<MINE_ESSENCE>();
        public ZElementVector<GM_GENERATOR_TYPE> gm_generator_type_array = new ZElementVector<GM_GENERATOR_TYPE>();
        public ZElementVector<GM_GENERATOR_ESSENCE> gm_generator_essence_array = new ZElementVector<GM_GENERATOR_ESSENCE>();
        public ZElementVector<FIREWORKS_ESSENCE> fireworks_essence_array = new ZElementVector<FIREWORKS_ESSENCE>();
        public ZElementVector<NPC_WAR_TOWERBUILD_SERVICE> npc_war_towerbuild_service_array = new ZElementVector<NPC_WAR_TOWERBUILD_SERVICE>();
        public ZElementVector<PLAYER_SECONDLEVEL_CONFIG> player_secondlevel_config_array = new ZElementVector<PLAYER_SECONDLEVEL_CONFIG>();
        public ZElementVector<NPC_RESETPROP_SERVICE> npc_resetprop_service_array = new ZElementVector<NPC_RESETPROP_SERVICE>();
        public ZElementVector<ESTONE_ESSENCE> estone_essence_array = new ZElementVector<ESTONE_ESSENCE>();
        public ZElementVector<PSTONE_ESSENCE> pstone_essence_array = new ZElementVector<PSTONE_ESSENCE>();
        public ZElementVector<SSTONE_ESSENCE> sstone_essence_array = new ZElementVector<SSTONE_ESSENCE>();
        public ZElementVector<RECIPEROLL_MAJOR_TYPE> reciperoll_major_type_array = new ZElementVector<RECIPEROLL_MAJOR_TYPE>();
        public ZElementVector<RECIPEROLL_SUB_TYPE> reciperoll_sub_type_array = new ZElementVector<RECIPEROLL_SUB_TYPE>();
        public ZElementVector<RECIPEROLL_ESSENCE> reciperoll_essence_array = new ZElementVector<RECIPEROLL_ESSENCE>();
        public ZElementVector<SUITE_ESSENCE> suite_essence_array = new ZElementVector<SUITE_ESSENCE>();
        public ZElementVector<DOUBLE_EXP_ESSENCE> double_exp_essence_array = new ZElementVector<DOUBLE_EXP_ESSENCE>();
        public ZElementVector<DESTROYING_ESSENCE> destroying_essence_array = new ZElementVector<DESTROYING_ESSENCE>();
        public ZElementVector<NPC_EQUIPBIND_SERVICE> npc_equipbind_service_array = new ZElementVector<NPC_EQUIPBIND_SERVICE>();
        public ZElementVector<NPC_EQUIPDESTROY_SERVICE> npc_equipdestroy_service_array = new ZElementVector<NPC_EQUIPDESTROY_SERVICE>();
        public ZElementVector<NPC_EQUIPUNDESTROY_SERVICE> npc_equipundestroy_service_array = new ZElementVector<NPC_EQUIPUNDESTROY_SERVICE>();
        public ZElementVector<NPC_ITEM_CASH_SERVICE> npc_item_cash_service_array = new ZElementVector<NPC_ITEM_CASH_SERVICE>();
        public ZElementVector<SKILLMATTER_ESSENCE> skillmatter_essence_array = new ZElementVector<SKILLMATTER_ESSENCE>();
        public ZElementVector<VEHICLE_ESSENCE> vehicle_essence_array = new ZElementVector<VEHICLE_ESSENCE>();
        public ZElementVector<COUPLE_JUMPTO_ESSENCE> couple_jumpto_essence_array = new ZElementVector<COUPLE_JUMPTO_ESSENCE>();
        public ZElementVector<LOTTERY_ESSENCE> lottery_essence_array = new ZElementVector<LOTTERY_ESSENCE>();
        public ZElementVector<CAMRECORDER_ESSENCE> camrecorder_essence_array = new ZElementVector<CAMRECORDER_ESSENCE>();
        public ZElementVector<TITLE_PROP_CONFIG> title_prop_config_array = new ZElementVector<TITLE_PROP_CONFIG>();
        public ZElementVector<SPECIAL_ID_CONFIG> special_id_config_array = new ZElementVector<SPECIAL_ID_CONFIG>();
        public ZElementVector<INSTANCE_CONFIG> instance_config_array = new ZElementVector<INSTANCE_CONFIG>();
        public ZElementVector<TEXT_FIREWORKS_ESSENCE> text_fireworks_essence_array = new ZElementVector<TEXT_FIREWORKS_ESSENCE>();
        public ZElementVector<AI_HELPER_ESSENCE> ai_helper_essence_array = new ZElementVector<AI_HELPER_ESSENCE>();
        public ZElementVector<PET_BEDGE_ESSENCE> pet_bedge_essence_array = new ZElementVector<PET_BEDGE_ESSENCE>();
        public ZElementVector<PET_FOOD_ESSENCE> pet_food_essence_array = new ZElementVector<PET_FOOD_ESSENCE>();
        public ZElementVector<PET_SKILL_ESSENCE> pet_skill_essence_array = new ZElementVector<PET_SKILL_ESSENCE>();
        public ZElementVector<PET_LIFEGEN_ESSENCE> pet_lifegen_essence_array = new ZElementVector<PET_LIFEGEN_ESSENCE>();
        public ZElementVector<CHANGESHAPE_ESSENCE> changeshape_essence_array = new ZElementVector<CHANGESHAPE_ESSENCE>();
        public ZElementVector<PET_ARMOR_ESSENCE> pet_armor_essence_array = new ZElementVector<PET_ARMOR_ESSENCE>();
        public ZElementVector<MERGE_RECIPE_ESSENCE> merge_recipe_essence_array = new ZElementVector<MERGE_RECIPE_ESSENCE>();
        public ZElementVector<ADV_TRANSROLL_ESSENCE> adv_transroll_essence_array = new ZElementVector<ADV_TRANSROLL_ESSENCE>();
        public ZElementVector<RADAR_ESSENCE> radar_essence_array = new ZElementVector<RADAR_ESSENCE>();
        public ZElementVector<LITERARY_DOUBLE_EXP> literary_double_exp_array = new ZElementVector<LITERARY_DOUBLE_EXP>();
        public ZElementVector<NEW_LOTTERY_ESSENCE> new_lottery_essence_array = new ZElementVector<NEW_LOTTERY_ESSENCE>();
        public ZElementVector<LOTTERY_BET_ITEM_ESSENCE> lottery_bet_item_essence_array = new ZElementVector<LOTTERY_BET_ITEM_ESSENCE>();
        public ZElementVector<STILETTO_TOOL_ESSENCE> stiletto_tool_essence_array = new ZElementVector<STILETTO_TOOL_ESSENCE>();
        public ZElementVector<STILETTO_HELPER_ESSENCE> stiletto_helper_essence_array = new ZElementVector<STILETTO_HELPER_ESSENCE>();
        public ZElementVector<INLAY_STONE_ESSENCE> inlay_stone_essence_array = new ZElementVector<INLAY_STONE_ESSENCE>();
        public ZElementVector<INLAY_HELPER_ESSENCE> inlay_helper_essence_array = new ZElementVector<INLAY_HELPER_ESSENCE>();
        public ZElementVector<ANTI_INLAY_ESSENCE> anti_inlay_essence_array = new ZElementVector<ANTI_INLAY_ESSENCE>();
        public ZElementVector<INLAY_CHARM_ESSENCE> inlay_charm_essence_array = new ZElementVector<INLAY_CHARM_ESSENCE>();
        public ZElementVector<TRANSFORM_STONE_ESSENCE> transform_stone_essence_array = new ZElementVector<TRANSFORM_STONE_ESSENCE>();
        public ZElementVector<TRANSFORM_HELPER_ESSENCE> transform_helper_essence_array = new ZElementVector<TRANSFORM_HELPER_ESSENCE>();
        public ZElementVector<TRANSFORM_RECIPE_ESSENCE> transform_recipe_essence_array = new ZElementVector<TRANSFORM_RECIPE_ESSENCE>();
        public List<ZTalk_Proc> talk_proc_array = new List<ZTalk_Proc>();
        public int tag1;
        public byte[] buff1 = new byte[10];
        public int t;
        public int tag2;
        public byte[] buff2 = new byte[10];
        public void load(string filename)
        {
            ZReader reader = new ZReader(filename);
            version = reader.readuint();
            timeType = reader.readi();
            reader.readObject(装备属性);
            reader.readObject(装备属性大类);
            reader.readObject(装备属性小类);
            reader.readObject(装备);
            reader.readObject(药品大类);
            reader.readObject(药品小类);
            reader.readObject(药品);
            reader.readObject(skilltome_sub_type_array);
            reader.readObject(skilltome_essence_array);
            reader.readObject(材料大类);
            reader.readObject(材料小类);
            reader.readObject(材料);
            reader.readObject(refine_ticket_essence_array);
            tag1 = reader.readi();
            int n = reader.readi();
            buff1 = reader.readbs(n);
            t = reader.readi();
            reader.readObject(传送门);
            reader.readObject(彩票);
            reader.readObject(townscroll_essence_array);
            reader.readObject(revivescroll_essence_array);
            reader.readObject(taskmatter_essence_array);
            reader.readObject(掉落类型);
            reader.readObject(掉落表);
            reader.readObject(NPC对话服务);
            reader.readObject(NPC出售服务);
            reader.readObject(NPC收购服务);
            reader.readObject(NPC发布任务);
            reader.readObject(NPC回收任务);
            reader.readObject(npc_task_matter_service_array);
            reader.readObject(npc_heal_service_array);
            reader.readObject(NPC传送服务);
            reader.readObject(npc_proxy_service_array);
            reader.readObject(npc_storage_service_array);
            reader.readObject(NPC类型);
            reader.readObject(NPC);
            reader.readObject(怪物类型);
            reader.readObject(怪物);

            tag2 = reader.readi();
            n = reader.readi();
            buff2 = reader.readbs(n);

            reader.readObject(recipe_major_type_array);
            reader.readObject(recipe_sub_type_array);
            reader.readObject(recipe_essence_array);
            reader.readObject(merge_recipe_essence_array);
            reader.readObject(enemy_faction_config_array);
            reader.readObject(character_class_config_array);
            reader.readObject(param_adjust_config_array);
            reader.readObject(taskdice_essence_array);
            reader.readObject(tasknormalmatter_essence_array);
            reader.readObject(player_levelexp_config_array);
            reader.readObject(mine_type_array);
            reader.readObject(mine_essence_array);
            reader.readObject(gm_generator_type_array);
            reader.readObject(gm_generator_essence_array);
            reader.readObject(fireworks_essence_array);
            reader.readObject(npc_war_towerbuild_service_array);
            reader.readObject(player_secondlevel_config_array);
            reader.readObject(npc_resetprop_service_array);
            reader.readObject(estone_essence_array);
            reader.readObject(pstone_essence_array);
            reader.readObject(sstone_essence_array);
            reader.readObject(reciperoll_major_type_array);
            reader.readObject(reciperoll_sub_type_array);
            reader.readObject(reciperoll_essence_array);
            reader.readObject(suite_essence_array);
            reader.readObject(double_exp_essence_array);
            reader.readObject(destroying_essence_array);
            reader.readObject(npc_equipbind_service_array);
            reader.readObject(npc_equipdestroy_service_array);
            reader.readObject(npc_equipundestroy_service_array);
            reader.readObject(npc_item_cash_service_array);
            reader.readObject(skillmatter_essence_array);
            reader.readObject(vehicle_essence_array);
            reader.readObject(couple_jumpto_essence_array);
            reader.readObject(lottery_essence_array);
            reader.readObject(camrecorder_essence_array);
            reader.readObject(title_prop_config_array);
            reader.readObject(special_id_config_array);
            reader.readObject(instance_config_array);
            reader.readObject(text_fireworks_essence_array);
            reader.readObject(ai_helper_essence_array);
            reader.readObject(pet_bedge_essence_array);
            reader.readObject(pet_food_essence_array);
            reader.readObject(pet_skill_essence_array);
            reader.readObject(pet_lifegen_essence_array);
            reader.readObject(changeshape_essence_array);
            reader.readObject(pet_armor_essence_array);
            reader.readObject(adv_transroll_essence_array);
            reader.readObject(radar_essence_array);
            reader.readObject(literary_double_exp_array);
            reader.readObject(new_lottery_essence_array);
            reader.readObject(lottery_bet_item_essence_array);
            reader.readObject(stiletto_tool_essence_array);
            reader.readObject(stiletto_helper_essence_array);
            reader.readObject(inlay_stone_essence_array);
            reader.readObject(inlay_helper_essence_array);
            reader.readObject(anti_inlay_essence_array);
            reader.readObject(inlay_charm_essence_array);
            reader.readObject(transform_stone_essence_array);
            reader.readObject(transform_helper_essence_array);
            reader.readObject(transform_recipe_essence_array);

            int num = reader.readi();
            for (int i = 0; i < num; i++)
            {
                talk_proc_array.Add(reader.readt<ZTalk_Proc>());
            }
            reader.close();
        }
        public void save(string filename)
        {
            ZWriter writer = new ZWriter(filename);
            writer.writeObject(version);
            writer.writeObject(timeType);
            writer.writeObject(装备属性);
            writer.writeObject(装备属性大类);
            writer.writeObject(装备属性小类);
            writer.writeObject(装备);
            writer.writeObject(药品大类);
            writer.writeObject(药品小类);
            writer.writeObject(药品);
            writer.writeObject(skilltome_sub_type_array);
            writer.writeObject(skilltome_essence_array);
            writer.writeObject(材料大类);
            writer.writeObject(材料小类);
            writer.writeObject(材料);
            writer.writeObject(refine_ticket_essence_array);

            writer.writeObject(tag1);
            writer.writeObject(buff1.Length);
            writer.writeObject(buff1);
            writer.writeObject(t);

            writer.writeObject(传送门);
            writer.writeObject(彩票);
            writer.writeObject(townscroll_essence_array);
            writer.writeObject(revivescroll_essence_array);
            writer.writeObject(taskmatter_essence_array);
            writer.writeObject(掉落类型);
            writer.writeObject(掉落表);
            writer.writeObject(NPC对话服务);
            writer.writeObject(NPC出售服务);
            writer.writeObject(NPC收购服务);
            writer.writeObject(NPC发布任务);
            writer.writeObject(NPC回收任务);
            writer.writeObject(npc_task_matter_service_array);
            writer.writeObject(npc_heal_service_array);
            writer.writeObject(NPC传送服务);
            writer.writeObject(npc_proxy_service_array);
            writer.writeObject(npc_storage_service_array);
            writer.writeObject(NPC类型);
            writer.writeObject(NPC);
            writer.writeObject(怪物类型);
            writer.writeObject(怪物);
            writer.writeObject(tag2);
            writer.writeObject(buff2.Length);
            writer.writeObject(buff2);
            writer.writeObject(recipe_major_type_array);
            writer.writeObject(recipe_sub_type_array);
            writer.writeObject(recipe_essence_array);
            writer.writeObject(merge_recipe_essence_array);
            writer.writeObject(enemy_faction_config_array);
            writer.writeObject(character_class_config_array);
            writer.writeObject(param_adjust_config_array);
            writer.writeObject(taskdice_essence_array);
            writer.writeObject(tasknormalmatter_essence_array);
            writer.writeObject(player_levelexp_config_array);
            writer.writeObject(mine_type_array);
            writer.writeObject(mine_essence_array);
            writer.writeObject(gm_generator_type_array);
            writer.writeObject(gm_generator_essence_array);
            writer.writeObject(fireworks_essence_array);
            writer.writeObject(npc_war_towerbuild_service_array);
            writer.writeObject(player_secondlevel_config_array);
            writer.writeObject(npc_resetprop_service_array);
            writer.writeObject(estone_essence_array);
            writer.writeObject(pstone_essence_array);
            writer.writeObject(sstone_essence_array);
            writer.writeObject(reciperoll_major_type_array);
            writer.writeObject(reciperoll_sub_type_array);
            writer.writeObject(reciperoll_essence_array);
            writer.writeObject(suite_essence_array);
            writer.writeObject(double_exp_essence_array);
            writer.writeObject(destroying_essence_array);
            writer.writeObject(npc_equipbind_service_array);
            writer.writeObject(npc_equipdestroy_service_array);
            writer.writeObject(npc_equipundestroy_service_array);
            writer.writeObject(npc_item_cash_service_array);
            writer.writeObject(skillmatter_essence_array);
            writer.writeObject(vehicle_essence_array);
            writer.writeObject(couple_jumpto_essence_array);
            writer.writeObject(lottery_essence_array);
            writer.writeObject(camrecorder_essence_array);
            writer.writeObject(title_prop_config_array);
            writer.writeObject(special_id_config_array);
            writer.writeObject(instance_config_array);
            writer.writeObject(text_fireworks_essence_array);
            writer.writeObject(ai_helper_essence_array);
            writer.writeObject(pet_bedge_essence_array);
            writer.writeObject(pet_food_essence_array);
            writer.writeObject(pet_skill_essence_array);
            writer.writeObject(pet_lifegen_essence_array);
            writer.writeObject(changeshape_essence_array);
            writer.writeObject(pet_armor_essence_array);
            writer.writeObject(adv_transroll_essence_array);
            writer.writeObject(radar_essence_array);
            writer.writeObject(literary_double_exp_array);
            writer.writeObject(new_lottery_essence_array);
            writer.writeObject(lottery_bet_item_essence_array);
            writer.writeObject(stiletto_tool_essence_array);
            writer.writeObject(stiletto_helper_essence_array);
            writer.writeObject(inlay_stone_essence_array);
            writer.writeObject(inlay_helper_essence_array);
            writer.writeObject(anti_inlay_essence_array);
            writer.writeObject(inlay_charm_essence_array);
            writer.writeObject(transform_stone_essence_array);
            writer.writeObject(transform_helper_essence_array);
            writer.writeObject(transform_recipe_essence_array);
            writer.writeObject(0);
            writer.close();
        }
    }
}

