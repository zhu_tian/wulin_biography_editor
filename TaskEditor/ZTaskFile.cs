﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;

namespace TaskEditor
{
    enum FINISHWAY
    {
        NONE,
        杀怪,
        收集道具,
        与NPC对话,
        到达区域,
        等待时间,
        未知1,
        未知2,
        护送
    }
    class ZHeader
    {
        public uint Magic = _CheckMahic;
        public int Version = 0x5B;
        public int TaskNum = 0;
        public const uint _CheckMahic= 0x93858361;
    }
    class ZTaskFile
    {
        public List<ZTask> zTasks { get; private set; } = new List<ZTask>();
        public ZHeader zHeader { get; private set; } = new ZHeader();

        public void load(string filename)
        {
            zTasks.Clear();
            ZReader zReader = new ZReader(filename);

            zReader.readObject(zHeader);
            if (zHeader.Magic!=ZHeader._CheckMahic)
            {
                throw new Exception("文件类型错误!");
            }
            int[] pos = new int[zHeader.TaskNum];
            for (int i = 0; i < pos.Length; i++)
            {
                pos[i] = zReader.readi();
            }
            int j = 0;
            foreach (var item in pos)
            {
                ZTASK401 t = new ZTASK401();

                zReader.seek(item);
                t.load(zReader);
                zTasks.Add(t);

                j++;
                //if (j > 100)
                //{
                //    break;
                //}
            }
            zReader.close();
        }
        public void save(string filename)
        {
            zHeader.TaskNum = zTasks.Count;
            ZWriter zw = new ZWriter(filename);
            zw.writeObject(zHeader);
            if (zTasks.Count > 0)
            {
                int[] pos = new int[zHeader.TaskNum];
                for (int i = 0; i < zTasks.Count; i++)
                {
                    zw.writeObject(255);
                }
                for (int i = 0; i < zTasks.Count; i++)
                {
                    pos[i] = (int)zw.POS;
                    ZTask t = zTasks[i];
                    t.save(zw);
                }
                zw.seek(12);
                for (int i = 0; i < zTasks.Count; i++)
                {
                    zw.writeObject(pos[i]);
                }

            }
            zw.close();
        }
    }
}
