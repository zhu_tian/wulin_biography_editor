﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TaskEditor
{
    public partial class ZEditorForm : Form
    {
        public string zValue { get; private set; }
        public ZEditorForm(string text, string value)
        {
            InitializeComponent();
            textBox1.Text = value;
            label1.Text = text;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            zValue = null;
            DialogResult = DialogResult.Cancel;
            Close();
        }
        private void button2_Click(object sender, EventArgs e)
        {
            zValue = textBox1.Text;
            if (checkBox1.Checked)
            {
                float v = float.Parse(textBox1.Text);

                byte[] data = BitConverter.GetBytes(v);
                int fvalue = BitConverter.ToInt32(data, 0);
                zValue = fvalue + "";
            }
            DialogResult = DialogResult.OK;
            Close();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
            {
                int v = int.Parse(textBox1.Text);

                byte[] data = BitConverter.GetBytes(v);
                float fvalue = BitConverter.ToSingle(data,0);
                textBox1.Text = fvalue + "";
            }
            else
            {
                float v = float.Parse(textBox1.Text);

                byte[] data = BitConverter.GetBytes(v);
                int fvalue = BitConverter.ToInt32(data, 0);
                textBox1.Text = fvalue + "";
            }
        }
    }
}
